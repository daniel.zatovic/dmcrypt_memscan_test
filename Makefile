TARGET=bgrep
CPPFLAGS=
CFLAGS=-O3 -g -Wall 
LDLIBS=
CC=gcc

SOURCES=$(wildcard bgrep.c)
OBJECTS=$(SOURCES:.c=.o)

all: $(TARGET)

$(TARGET): $(OBJECTS)
	$(CC) -o $@ $^ $(LDLIBS)

clean:
	rm -f *.o *~ $(TARGET)

.PHONY: clean

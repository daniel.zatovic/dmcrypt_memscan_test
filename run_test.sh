#!/bin/bash

IP=192.168.56.105
VM_NAME=fedora2934
rm -f before mounted closed
SUSPEND=0
DEVICE=/dev/sdb
FORMAT_ARGS="--type luks2"

show_help() {
	echo "-h, -i <IP>, -v < VirtualBox VM name>, -d <VM device path>, -f <cryptsetup format arguments>"
}

while getopts "h?i:d:f:v:s" opt; do
    case "$opt" in
    h|\?)
        show_help
        exit 0
        ;;
    i)  IP=$OPTARG
        ;;
    d)  DEVICE=$OPTARG
        ;;
    v)  VM_NAME=$OPTARG
        ;;
    s)  SUSPEND=1
        ;;
    f)  FORMAT_ARGS=$OPTARG
        ;;
    esac
done

if [ ! -f ./bgrep ]; then
	make
fi

echo $DEVICE > .filename
scp .filename root@$IP:~
echo $FORMAT_ARGS > .format_args
scp .format_args root@$IP:~

ssh root@$IP 'bash -s' <<'ENDSSH'
	device=`cat .filename`
	echo "secret" > key
	sudo cryptsetup luksFormat `cat .format_args` -q --key-file=key $device
	sudo cryptsetup luksOpen -q --key-file=key $device key_test
ENDSSH

VBoxManage debugvm $VM_NAME dumpvmcore --filename=mounted

if [ $SUSPEND -eq 1 ]; then
	echo "Suspending mounted device"
	ssh root@$IP 'sudo cryptsetup luksSuspend key_test'
else
	echo "Closing mounted device"
	ssh root@$IP 'sudo cryptsetup luksClose key_test'
fi
VBoxManage debugvm $VM_NAME dumpvmcore --filename=closed


if [ $SUSPEND -eq 1 ]; then
ssh root@$IP 'bash -s' <<'ENDSSH'
	sudo cryptsetup luksResume -q --key-file=key key_test
	arr=(`sudo dmsetup table --showkey /dev/mapper/key_test`)
	echo ${arr[4]} > .key
	sudo cryptsetup luksClose key_test
ENDSSH
else
ssh root@$IP 'bash -s' <<'ENDSSH'
	sudo cryptsetup luksOpen -q --key-file=key `cat .filename` key_test
	arr=(`sudo dmsetup table --showkey /dev/mapper/key_test`)
	echo ${arr[4]} > .key
	sudo cryptsetup luksClose key_test
ENDSSH
fi

scp root@$IP:.key .

KEY=`cat .key`
SUBKEY_LENGTH=8
KEY_LENGTH=${#KEY}
SUBKEY_TMPFILE=/tmp/subkeys$$

for i in $(seq 0 $SUBKEY_LENGTH $((KEY_LENGTH-SUBKEY_LENGTH+1))); do
	echo -n "${KEY:i:SUBKEY_LENGTH}" >> $SUBKEY_TMPFILE
done
#cat $SUBKEY_TMPFILE


for file in mounted closed; do
	echo "checking $file"
	echo "== Checking $file =="
	echo -n "Whole hex key found: "
	grep -c -F `cat .key` $file
	echo -n "Hex subkeys of length $SUBKEY_LENGTH found: "
	grep -c -Ff $SUBKEY_TMPFILE $file 
	echo -n "Whole binary key found: "
	./bgrep `cat .key` $file

	for i in $(seq 0 $SUBKEY_LENGTH $((KEY_LENGTH-SUBKEY_LENGTH+1))); do
		SUBKEY=${KEY:i:SUBKEY_LENGTH}
		echo -n "Binary subkey '$SUBKEY' found: "
		./bgrep $SUBKEY $file
	done

	aeskeyfind $file
done
